package com.crustlab.githubsearch.app

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.crustlab.githubsearch.TestsData
import com.crustlab.githubsearch.di.testsDataModule
import com.crustlab.githubsearch.domain.models.EmptyQueryError
import com.crustlab.githubsearch.domain.models.PaginatedList
import com.crustlab.githubsearch.domain.platform.SchedulersProvider
import com.crustlab.githubsearch.domain.usecase.SearchRepositoriesUseCase
import com.crustlab.githubsearch.ui.main.SearchViewModel
import com.crustlab.githubsearch.ui.main.list.SearchViewItem
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.rxjava3.core.Single
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SearchViewModelTests : KoinTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val searchRepositoriesUseCase = mock<SearchRepositoriesUseCase>()
    private val schedulersProvider: SchedulersProvider by inject()

    @Before
    fun before() {
        startKoin {
            modules(
                listOf(
                    testsDataModule
                )
            )
        }
    }

    @After
    fun after() {
        stopKoin()
    }


    @Test
    fun `should emit error on empty query`() {
        val searchViewModel = SearchViewModel(
            searchRepositoriesUseCase = searchRepositoriesUseCase,
            schedulersProvider = schedulersProvider
        )

        val query = ""
        val page = 1

        `when`(searchRepositoriesUseCase.execute(query, page)).thenReturn(Single.error(EmptyQueryError))

        searchViewModel.error.test().awaitValue().assertValue { it is EmptyQueryError }
    }

    @Test
    fun `should emit 2 viewItems (repository and pagination progress)  with progress on 'test' query`() {
        val searchViewModel = SearchViewModel(
            searchRepositoriesUseCase = searchRepositoriesUseCase,
            schedulersProvider = schedulersProvider
        )

        val query = "test"
        val page = 1

        `when`(searchRepositoriesUseCase.execute(query, page)).thenReturn(
            Single.just(
                PaginatedList(
                    query,
                    2L,
                    listOf(TestsData.mockMikeRepository)
                )
            )
        )

        searchViewModel.onQuery(query)
        searchViewModel.viewItems.test().awaitValue().assertValue {
            it.size == 2 &&
                    (it.first() as SearchViewItem.Item).repository.name == "mike" &&
                    it[1] is SearchViewItem.Progress
        }
    }

    @Test
    fun `should emit single viewItem on 'test' query`() {
        val searchViewModel = SearchViewModel(
            searchRepositoriesUseCase = searchRepositoriesUseCase,
            schedulersProvider = schedulersProvider
        )

        val query = "test"
        val page = 1

        `when`(searchRepositoriesUseCase.execute(query, page)).thenReturn(
            Single.just(
                PaginatedList(
                    query,
                    1L,
                    listOf(TestsData.mockMikeRepository)
                )
            )
        )

        searchViewModel.onQuery(query)
        searchViewModel.viewItems.test().awaitValue().assertValue {
            it.size == 1 &&
                    (it.first() as SearchViewItem.Item).repository.name == "mike"
        }
    }

}