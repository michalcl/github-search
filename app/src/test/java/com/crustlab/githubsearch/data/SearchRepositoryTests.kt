package com.crustlab.githubsearch.data

import com.crustlab.githubsearch.TestsData
import com.crustlab.githubsearch.data.networking.GitHubService
import com.crustlab.githubsearch.data.repositories.SearchRepositoryImpl
import com.crustlab.githubsearch.domain.models.EmptyQueryError
import com.crustlab.githubsearch.domain.models.PaginatedList
import com.crustlab.githubsearch.domain.models.Repository
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.exceptions.CompositeException
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Test
import org.mockito.Mockito.`when`
import retrofit2.HttpException
import retrofit2.Response

class SearchRepositoryTests {

    private val gitHubService = mock<GitHubService>()

    @Test
    fun `should return error EmptyQuery on send empty query string`() {
        val repository = SearchRepositoryImpl(
            gitHubService = gitHubService
        )
        val query = ""
        val page = 0
        val response = Response.error<PaginatedList<Repository>>(
            422,
            "content".toResponseBody("application/json".toMediaTypeOrNull())
        )

        `when`(gitHubService.searchRepositories(query, page)).thenReturn(Single.error(HttpException(response)))

        repository.searchRepositories(query, page).test().assertError {
            (it as CompositeException).exceptions[0] is EmptyQueryError
        }
    }

    @Test
    fun `should return list of repositories on send 'mike' query string`() {
        val repository = SearchRepositoryImpl(
            gitHubService = gitHubService
        )
        val query = "mike"
        val page = 0

        `when`(gitHubService.searchRepositories(query, page)).thenReturn(Single.just(TestsData.mockRepositoryResponse))

        repository.searchRepositories(query, page).test().assertValue {
            it.totalCount == 1L &&
                    it.items.size == 1 &&
                    it.items.first().name == TestsData.mockMikeRepositoryDto.name
        }
    }

}