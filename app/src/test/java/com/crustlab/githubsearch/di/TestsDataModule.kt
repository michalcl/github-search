package com.crustlab.githubsearch.di

import com.crustlab.githubsearch.domain.platform.SchedulersProvider
import org.koin.dsl.module

val testsDataModule = module {

    single<SchedulersProvider> {
        TestsSchedulersProviderImpl()
    }

}

