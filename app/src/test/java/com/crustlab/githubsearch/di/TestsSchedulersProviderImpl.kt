package com.crustlab.githubsearch.di

import com.crustlab.githubsearch.domain.platform.SchedulersProvider
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers

class TestsSchedulersProviderImpl() : SchedulersProvider {

    override val main: Scheduler = Schedulers.trampoline()

    override val io: Scheduler = Schedulers.trampoline()

    override val computation: Scheduler = Schedulers.trampoline()

}