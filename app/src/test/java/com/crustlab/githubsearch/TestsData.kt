package com.crustlab.githubsearch

import com.crustlab.githubsearch.data.models.OwnerDto
import com.crustlab.githubsearch.data.models.RepositoriesResponse
import com.crustlab.githubsearch.data.models.RepositoryDto
import com.crustlab.githubsearch.data.models.toDomainModel

// TODO example test data provider
object TestsData {

    val mockMikeRepositoryDto = RepositoryDto(
        id = 0,
        name = "mike",
        fullName = "mike full name", htmlUrl = "url",
        owner = OwnerDto(
            id = 0
        )
    )

    val mockRepositoryResponse = RepositoriesResponse(
        totalCount = 1,
        incompleteResults = false,
        items = listOf(mockMikeRepositoryDto)
    )

    val mockMikeRepository = mockMikeRepositoryDto.toDomainModel()

}