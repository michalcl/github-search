package com.crustlab.githubsearch.data

import com.crustlab.githubsearch.Constants.BASE_API_URL
import com.crustlab.githubsearch.data.networking.GitHubService
import com.crustlab.githubsearch.data.platform.SchedulersProviderImpl
import com.crustlab.githubsearch.data.repositories.SearchRepository
import com.crustlab.githubsearch.data.repositories.SearchRepositoryImpl
import com.crustlab.githubsearch.domain.platform.SchedulersProvider
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val dataModule = module {

    single<SchedulersProvider> {
        SchedulersProviderImpl()
    }

}

val networkModule = module {

    single {
        Gson()
    }

    single {
        OkHttpClient.Builder()
            // logger
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            // GitHub 'Accept' header
            .addInterceptor {
                val request = it.request().newBuilder().apply {
                    header("Accept", "application/vnd.github.v3+json")
                }.build()
                it.proceed(request)
            }
            .build()
    }

    single {
        Retrofit.Builder()
            .baseUrl(BASE_API_URL)
            .client(get())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(get()))
            .build()
    }

    single {
        get<Retrofit>().create(GitHubService::class.java)
    }

}

val repositoryModule = module {

    factory<SearchRepository> {
        SearchRepositoryImpl(
            gitHubService = get()
        )
    }

}