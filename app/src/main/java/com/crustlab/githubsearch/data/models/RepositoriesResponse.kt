package com.crustlab.githubsearch.data.models

import com.crustlab.githubsearch.domain.models.PaginatedList
import com.crustlab.githubsearch.domain.models.Repository
import com.google.gson.annotations.SerializedName

/**
 * @param incomplete_results
 * To keep the Search API fast for everyone, we limit how long any individual query can run.
 * For queries that exceed the time limit, the API returns the matches that were already found prior to the timeout,
 * and the response has the incomplete_results property set to true.
 */
data class RepositoriesResponse(
    @SerializedName("total_count") val totalCount: Long,
    @SerializedName("incomplete_results") val incompleteResults: Boolean,
    @SerializedName("items") val items: List<RepositoryDto>
)

data class RepositoryDto(
    @SerializedName("id") val id: Long,
    @SerializedName("name") val name: String,
    @SerializedName("full_name") val fullName: String,
    @SerializedName("html_url") val htmlUrl: String,
    @SerializedName("owner") val owner: OwnerDto
)

data class OwnerDto(
    @SerializedName("id") val id: Long
)

fun RepositoriesResponse.toDomainModel(query: String) = PaginatedList(
    query = query,
    totalCount = totalCount,
    items = items.map { it.toDomainModel() }
)

fun RepositoryDto.toDomainModel() = Repository(
    id = id,
    ownerId = owner.id,
    name = name,
    fullName = fullName,
    url = htmlUrl
)