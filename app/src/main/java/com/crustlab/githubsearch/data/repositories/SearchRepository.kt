package com.crustlab.githubsearch.data.repositories

import android.util.Log
import com.crustlab.githubsearch.data.models.toDomainModel
import com.crustlab.githubsearch.data.networking.GitHubService
import com.crustlab.githubsearch.domain.models.EmptyQueryError
import com.crustlab.githubsearch.domain.models.PaginatedList
import com.crustlab.githubsearch.domain.models.Repository
import io.reactivex.rxjava3.core.Single
import retrofit2.HttpException

interface SearchRepository {

    fun searchRepositories(query: String, page: Int): Single<PaginatedList<Repository>>

}

// in more advanced architecture with modules instead packages in app, it should be internal class
class SearchRepositoryImpl(
    private val gitHubService: GitHubService
) : SearchRepository {

    override fun searchRepositories(query: String, page: Int): Single<PaginatedList<Repository>> {
        return gitHubService.searchRepositories(
            query = query,
            page = page
        )
            .map { it.toDomainModel(query) }
            .onErrorResumeNext { error ->
                // parse error body, prepare correct domain error
                (error as? HttpException)?.let {
                    if (it.code() == 422) {
                        return@onErrorResumeNext Single.error(EmptyQueryError)
                    }
                }
                Single.error(error)
            }
            .doOnError { Log.e("SearchRepositoryImpl", "error while fetching repositories", it) }
    }

}