package com.crustlab.githubsearch

import android.app.Application
import com.crustlab.githubsearch.data.dataModule
import com.crustlab.githubsearch.data.networkModule
import com.crustlab.githubsearch.data.repositoryModule
import com.crustlab.githubsearch.domain.useCaseModule
import com.crustlab.githubsearch.ui.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            if (BuildConfig.DEBUG) {
                androidLogger(Level.INFO)
            }
            androidContext(this@App)
            modules(
                dataModule,
                networkModule,
                repositoryModule,
                useCaseModule,
                viewModelModule
            )
        }
    }
}