package com.crustlab.githubsearch.ui.utils

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

internal inline fun scrollIdleListener(crossinline onIdle: (lastPosition: Int) -> Unit): RecyclerViewScrollIdleListener =
    object : RecyclerViewScrollIdleListener() {
        override fun onIdle(lastPosition: Int) = onIdle(lastPosition)
    }

abstract class RecyclerViewScrollIdleListener : RecyclerView.OnScrollListener() {

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
            (recyclerView.layoutManager as? LinearLayoutManager)?.let {
                onIdle(it.findLastVisibleItemPosition())
            }
        }
    }

    abstract fun onIdle(lastPosition: Int)
}