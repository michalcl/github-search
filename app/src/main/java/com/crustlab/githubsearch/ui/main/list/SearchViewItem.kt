package com.crustlab.githubsearch.ui.main.list

import com.crustlab.githubsearch.domain.models.Repository

sealed class SearchViewItem(
    val type: Type
) {

    data class Item(val repository: Repository) : SearchViewItem(Type.ITEM)

    object Progress : SearchViewItem(Type.PROGRESS)

    enum class Type {
        ITEM,
        PROGRESS
    }

}
