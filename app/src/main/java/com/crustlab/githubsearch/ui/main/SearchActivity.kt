package com.crustlab.githubsearch.ui.main

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import com.crustlab.githubsearch.R
import com.crustlab.githubsearch.databinding.ActivitySearchBinding
import com.crustlab.githubsearch.domain.models.EmptyQueryError
import com.crustlab.githubsearch.domain.models.Repository
import com.crustlab.githubsearch.ui.main.list.SearchAdapter
import com.crustlab.githubsearch.ui.main.list.SearchViewItem
import com.crustlab.githubsearch.ui.utils.scrollIdleListener
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchActivity : AppCompatActivity() {

    private val searchViewModel: SearchViewModel by viewModel()
    private val listScrollListener = createListScrollListener()
    private lateinit var binding: ActivitySearchBinding
    private var adapter: SearchAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
        initObservers()
    }

    override fun onDestroy() {
        with(binding.recyclerView) {
            adapter = null
            removeOnScrollListener(listScrollListener)
        }
        adapter = null
        super.onDestroy()
    }

    private fun initView() = with(binding) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean = true

            override fun onQueryTextChange(newText: String?): Boolean {
                searchViewModel.onQuery(newText ?: "")
                return true
            }
        })

        if (adapter == null) adapter = SearchAdapter(this@SearchActivity) { searchViewModel.onRepositoryClick(it) }
        if (recyclerView.adapter == null) recyclerView.adapter = adapter
        recyclerView.addOnScrollListener(listScrollListener)
    }

    private fun initObservers() {
        searchViewModel.openWebRepository.observe(this) { onOpenWebRepository(it) }
        searchViewModel.error.observe(this) { onError(it) }
        searchViewModel.viewItems.observe(this) { onViewItems(it) }
    }

    private fun onOpenWebRepository(repository: Repository) {
        val intent = Intent.makeMainSelectorActivity(Intent.ACTION_MAIN, Intent.CATEGORY_APP_BROWSER).apply {
            data = Uri.parse(repository.url)
        }
        startActivity(intent)
    }

    private fun onError(error: Throwable) {
        when (error) {
            is EmptyQueryError -> {
                with(binding.infoPlaceholderTextView) {
                    setText(R.string.empty_query)
                    isVisible = true
                }
                adapter?.submitList(emptyList())
            }
            else -> {
                binding.infoPlaceholderTextView.isVisible = false
                Snackbar.make(
                    binding.mainActivityLayout,
                    getString(R.string.something_went_wrong),
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun onViewItems(items: List<SearchViewItem>) {
        if (items.isEmpty()) {
            with(binding.infoPlaceholderTextView) {
                setText(R.string.empty_results)
                isVisible = true
            }
        } else {
            binding.infoPlaceholderTextView.isVisible = false
        }
        adapter?.submitList(items)
    }

    private fun createListScrollListener() = scrollIdleListener() { searchViewModel.onScroll(it) }

}