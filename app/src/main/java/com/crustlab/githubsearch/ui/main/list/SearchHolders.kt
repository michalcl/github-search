package com.crustlab.githubsearch.ui.main.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.crustlab.githubsearch.databinding.HolderSearchProgressBinding
import com.crustlab.githubsearch.databinding.HolderSearchRepositoryBinding
import com.crustlab.githubsearch.domain.models.Repository

object SearchHolders {

    abstract class Base(viewItem: View) : RecyclerView.ViewHolder(viewItem) {
        open fun bind(item: SearchViewItem) = Unit
    }

    class RepositoryHolder private constructor(
        private val binding: HolderSearchRepositoryBinding,
        private val onClick: (Repository) -> Unit
    ) : Base(binding.root) {

        override fun bind(item: SearchViewItem) {
            val repository = (item as SearchViewItem.Item).repository
            with(binding) {
                root.setOnClickListener { onClick(repository) }
                nameTextView.text = repository.name
                fullNameTextView.text = repository.fullName
            }
        }

        companion object {

            fun create(
                inflater: LayoutInflater,
                parent: ViewGroup,
                onClick: (Repository) -> Unit
            ) = RepositoryHolder(
                binding = HolderSearchRepositoryBinding.inflate(
                    inflater,
                    parent,
                    false
                ),
                onClick = onClick
            )

        }

    }

    class ProgressHolder private constructor(
        private val binding: HolderSearchProgressBinding
    ) : Base(binding.root) {

        companion object {

            fun create(
                inflater: LayoutInflater,
                parent: ViewGroup
            ) = ProgressHolder(
                binding = HolderSearchProgressBinding.inflate(
                    inflater,
                    parent,
                    false
                )
            )
        }

    }

}