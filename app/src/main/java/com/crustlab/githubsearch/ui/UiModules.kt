package com.crustlab.githubsearch.ui

import com.crustlab.githubsearch.ui.main.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel {
        SearchViewModel(
            searchRepositoriesUseCase = get(),
            schedulersProvider = get()
        )
    }

}