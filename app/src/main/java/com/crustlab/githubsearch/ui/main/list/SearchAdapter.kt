package com.crustlab.githubsearch.ui.main.list

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.crustlab.githubsearch.domain.models.Repository

class SearchAdapter(
    context: Context,
    private val onRepositoryClick: (Repository) -> Unit
) : ListAdapter<SearchViewItem, SearchHolders.Base>(DiffCallback) {

    private val layoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchHolders.Base =
        when (viewType) {
            SearchViewItem.Type.ITEM.ordinal ->
                SearchHolders.RepositoryHolder.create(
                    inflater = layoutInflater,
                    parent = parent,
                    onClick = onRepositoryClick
                )
            SearchViewItem.Type.PROGRESS.ordinal ->
                SearchHolders.ProgressHolder.create(
                    inflater = layoutInflater,
                    parent = parent
                )
            else -> throw IllegalArgumentException("SearchAdapter, unsupported view type")
        }

    override fun onBindViewHolder(holder: SearchHolders.Base, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int = getItem(position).type.ordinal

    object DiffCallback : DiffUtil.ItemCallback<SearchViewItem>() {
        override fun areItemsTheSame(oldItem: SearchViewItem, newItem: SearchViewItem): Boolean = when {
            oldItem is SearchViewItem.Item && newItem is SearchViewItem.Item ->
                oldItem.repository.id == newItem.repository.id &&
                        oldItem.repository.ownerId == newItem.repository.ownerId
            else -> false
        }

        override fun areContentsTheSame(oldItem: SearchViewItem, newItem: SearchViewItem): Boolean =
            oldItem == newItem

    }

}