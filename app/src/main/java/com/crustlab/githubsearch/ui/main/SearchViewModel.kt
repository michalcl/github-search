package com.crustlab.githubsearch.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.crustlab.githubsearch.Constants.DEFAULT_PAGINATION_OFFSET
import com.crustlab.githubsearch.Constants.SEARCH_DEBOUNCE_MILLIS
import com.crustlab.githubsearch.domain.models.PaginatedList
import com.crustlab.githubsearch.domain.models.Repository
import com.crustlab.githubsearch.domain.platform.SchedulersProvider
import com.crustlab.githubsearch.domain.usecase.SearchRepositoriesUseCase
import com.crustlab.githubsearch.ui.main.list.SearchViewItem
import com.crustlab.githubsearch.ui.utils.LiveEvent
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.processors.BehaviorProcessor
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class SearchViewModel(
    private val searchRepositoriesUseCase: SearchRepositoriesUseCase,
    private val schedulersProvider: SchedulersProvider
) : ViewModel() {

    private val _viewItems = MutableLiveData<List<SearchViewItem>>()
    val viewItems: LiveData<List<SearchViewItem>> = _viewItems

    private val _error = LiveEvent<Throwable>()
    val error: LiveData<Throwable> = _error

    private val _openWebRepository = LiveEvent<Repository>()
    val openWebRepository: LiveData<Repository> = _openWebRepository

    private val disposables = CompositeDisposable()
    private val searchProcessor = BehaviorProcessor.create<String>()
    private val repositories = mutableListOf<Repository>()
    private var fetchDisposable: Disposable? = null
    private var searchQuery = ""
    private var lastSearchQuery = ""
    private var nextPage: Int = 1
    private var hasMore = true
    private var fetching = false

    init {
        searchProcessor.debounce(SEARCH_DEBOUNCE_MILLIS, TimeUnit.MILLISECONDS, Schedulers.computation())
            .subscribeBy(
                onNext = {
                    fetchDisposable?.dispose()
                    nextPage = 1
                    searchRepositories(it)
                },
                onError = { Log.e("SearchViewModel", "debounce error", it) }
            )
            .addTo(disposables)

        onQuery(searchQuery)
    }

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

    fun onRepositoryClick(repository: Repository) {
        _openWebRepository.postValue(repository)
    }

    fun onQuery(query: String) {
        searchQuery = query
        searchProcessor.onNext(query)
    }

    fun onScroll(lastPosition: Int) {
        if (lastPosition + DEFAULT_PAGINATION_OFFSET < repositories.size || fetching || !hasMore) return
        fetchDisposable?.dispose()
        searchRepositories(lastSearchQuery)
    }

    private fun searchRepositories(query: String) {
        // TODO should we call api if query is empty?
        fetching = true
        fetchDisposable = searchRepositoriesUseCase.execute(
            query = query,
            page = nextPage
        )
            .subscribeOn(schedulersProvider.io)
            .observeOn(schedulersProvider.main)
            .subscribeBy(
                onSuccess = ::onSuccess,
                onError = ::onError
            )
            .addTo(disposables)
    }

    private fun onSuccess(data: PaginatedList<Repository>) {
        val tmpList = mutableListOf<Repository>()
        // add all to tmpList
        if (nextPage != 1) tmpList.addAll(repositories)
        tmpList.addAll(data.items)
        // distinct tmp because api sometimes returning same element and replace main repositories list
        repositories.clear()
        repositories.addAll(tmpList.distinct())
        lastSearchQuery = data.query
        hasMore = data.totalCount > repositories.size
        nextPage++
        prepareViewItems()
        fetching = false
    }

    private fun onError(error: Throwable) {
        // TODO improve error handling:
        //  - hide progress at the bottom? add retry button?
        //  - there is a possibility to keep old list on fetching more items, changing fast query which failing
        _error.postValue(error)
        fetching = false
    }

    private fun prepareViewItems() {
        val viewItems = mutableListOf<SearchViewItem>()

        viewItems.addAll(repositories.map { SearchViewItem.Item(it) })
        if (hasMore) viewItems.add(SearchViewItem.Progress)

        _viewItems.postValue(viewItems)
    }

}