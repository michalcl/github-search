package com.crustlab.githubsearch

object Constants {
    const val BASE_API_URL = "https://api.github.com/"
    const val SEARCH_DEBOUNCE_MILLIS = 500L
    const val DEFAULT_PAGINATION_OFFSET = 5
}