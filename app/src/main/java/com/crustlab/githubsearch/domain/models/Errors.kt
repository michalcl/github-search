package com.crustlab.githubsearch.domain.models

object EmptyQueryError: RuntimeException()