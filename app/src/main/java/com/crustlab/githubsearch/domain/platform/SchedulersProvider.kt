package com.crustlab.githubsearch.domain.platform

import io.reactivex.rxjava3.core.Scheduler

interface SchedulersProvider {

    val main: Scheduler

    val io: Scheduler

    val computation: Scheduler

}