package com.crustlab.githubsearch.domain.usecase

import com.crustlab.githubsearch.data.repositories.SearchRepository
import com.crustlab.githubsearch.domain.models.PaginatedList
import com.crustlab.githubsearch.domain.models.Repository
import io.reactivex.rxjava3.core.Single

class SearchRepositoriesUseCase(
    private val searchRepository: SearchRepository
) {

    fun execute(query: String, page: Int): Single<PaginatedList<Repository>> {
        return searchRepository.searchRepositories(
            query = query,
            page = page
        )
    }

}