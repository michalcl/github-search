package com.crustlab.githubsearch.domain.models

data class Repository(
    val id: Long,
    val ownerId: Long,
    val name: String,
    val fullName: String,
    val url: String
)
