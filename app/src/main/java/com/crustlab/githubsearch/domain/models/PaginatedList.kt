package com.crustlab.githubsearch.domain.models

data class PaginatedList<T>(
    val query: String,
    val totalCount: Long,
    val items: List<T>
)
