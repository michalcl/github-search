package com.crustlab.githubsearch.domain

import com.crustlab.githubsearch.domain.usecase.SearchRepositoriesUseCase
import org.koin.dsl.module

val useCaseModule = module {

    factory {
        SearchRepositoriesUseCase(
            searchRepository = get()
        )
    }

}